<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registrasi()
    {
        return view('Halaman.registrasi');
    }

    public function Dasboard(Request $request)
    {
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];
        return view('Halaman.home', ['namaDepan' => $namaDepan, 'namaBelakang' =>$namaBelakang]); 
    }
}
